module Gitlab
  module Triage
    module FilterBuilders
      class BaseFilterBuilder
        attr_reader :filter_name, :filter_contents

        def initialize(filter_name, filter_contents)
          @filter_name = filter_name
          @filter_contents = filter_contents
        end

        def build_filter
          "&#{filter_name}=#{filter_content}"
        end
      end
    end
  end
end
