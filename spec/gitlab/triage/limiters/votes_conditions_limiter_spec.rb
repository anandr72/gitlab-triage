require 'spec_helper'

require 'gitlab/triage/limiters/votes_conditions_limiter'

describe Gitlab::Triage::Limiters::VotesConditionsLimiter do
  let(:upvotes) { 5 }
  let(:downvotes) { 1 }

  let(:resource) do
    {
      upvotes: upvotes,
      downvotes: downvotes
    }
  end
  let(:condition) do
    {
      attribute: 'upvotes',
      condition: 'greater_than',
      threshold: 3
    }
  end

  subject { described_class.new(resource, condition) }

  it_behaves_like 'a limiter'

  context '#resource_value' do
    it 'has the correct value for upvotes attribute' do
      expect(subject.resource_value).to eq(upvotes)
    end

    it 'has the correct value for the downvotes attribute' do
      limiter = described_class.new(resource, condition.merge(attribute: 'downvotes'))
      expect(limiter.resource_value).to eq(downvotes)
    end
  end

  context '#condition_value' do
    it 'has the correct value for comparison' do
      expect(subject.condition_value).to eq(3)
    end
  end

  context '#calculate' do
    it 'calculates true given correct condition' do
      expect(subject.calculate).to eq(true)
    end

    it 'calculate false given wrong condition' do
      limiter = described_class.new(resource, condition.merge(condition: 'less_than'))
      expect(limiter.calculate).to eq(false)
    end
  end
end
