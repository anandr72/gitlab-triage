require_relative 'base_filter_builder'

module Gitlab
  module Triage
    module FilterBuilders
      class MultiFilterBuilder < BaseFilterBuilder
        attr_reader :separator

        def initialize(filter_name, filter_contents, separator)
          @separator = separator
          super(filter_name, filter_contents)
        end

        def filter_content
          filter_contents.join(separator)
        end
      end
    end
  end
end
